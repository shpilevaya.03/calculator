﻿namespace Calculator
{
    partial class frmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.egoldsFormStyle1 = new Calculator.Components.EgoldsFormStyle(this.components);
            this.equelButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.multiplicationButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.listButton = new System.Windows.Forms.Button();
            this.moreDetailedButton = new System.Windows.Forms.Button();
            this.hintsToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.firstNumTxtBox = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.firstNumLabel = new System.Windows.Forms.Label();
            this.numSystemTxtBox = new System.Windows.Forms.TextBox();
            this.numSystemLabel = new System.Windows.Forms.Label();
            this.secondNumLabel = new System.Windows.Forms.Label();
            this.secondNumTxtBox = new System.Windows.Forms.TextBox();
            this.resultTxtBox = new System.Windows.Forms.TextBox();
            this.miniTxtBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // egoldsFormStyle1
            // 
            this.egoldsFormStyle1.AllowUserResize = false;
            this.egoldsFormStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.egoldsFormStyle1.ContextMenuForm = null;
            this.egoldsFormStyle1.ControlBoxButtonsWidth = 40;
            this.egoldsFormStyle1.EnableControlBoxIconsLight = false;
            this.egoldsFormStyle1.EnableControlBoxMouseLight = false;
            this.egoldsFormStyle1.Form = this;
            this.egoldsFormStyle1.FormStyle = Calculator.Components.EgoldsFormStyle.fStyle.UserStyle;
            this.egoldsFormStyle1.HeaderColor = System.Drawing.Color.CornflowerBlue;
            this.egoldsFormStyle1.HeaderColorAdditional = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.egoldsFormStyle1.HeaderColorGradientEnable = true;
            this.egoldsFormStyle1.HeaderColorGradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.egoldsFormStyle1.HeaderHeight = 30;
            this.egoldsFormStyle1.HeaderImage = null;
            this.egoldsFormStyle1.HeaderTextColor = System.Drawing.Color.White;
            this.egoldsFormStyle1.HeaderTextFont = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(150)));
            // 
            // equelButton
            // 
            this.equelButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.equelButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.equelButton.FlatAppearance.BorderSize = 0;
            this.equelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.equelButton.Font = new System.Drawing.Font("Malgun Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equelButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.equelButton.Location = new System.Drawing.Point(364, 282);
            this.equelButton.Name = "equelButton";
            this.equelButton.Size = new System.Drawing.Size(85, 60);
            this.equelButton.TabIndex = 1;
            this.equelButton.Text = "=";
            this.equelButton.UseVisualStyleBackColor = false;
            this.equelButton.Click += new System.EventHandler(this.equelButton_Click);
            // 
            // plusButton
            // 
            this.plusButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.plusButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.plusButton.FlatAppearance.BorderSize = 0;
            this.plusButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plusButton.Font = new System.Drawing.Font("Malgun Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.plusButton.Location = new System.Drawing.Point(275, 282);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(85, 60);
            this.plusButton.TabIndex = 5;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = false;
            this.plusButton.Click += new System.EventHandler(this.plusButton_Click);
            // 
            // minusButton
            // 
            this.minusButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.minusButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.minusButton.FlatAppearance.BorderSize = 0;
            this.minusButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.minusButton.Font = new System.Drawing.Font("Malgun Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minusButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.minusButton.Location = new System.Drawing.Point(186, 282);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(85, 60);
            this.minusButton.TabIndex = 9;
            this.minusButton.Text = "-";
            this.minusButton.UseVisualStyleBackColor = false;
            this.minusButton.Click += new System.EventHandler(this.minusButton_Click);
            // 
            // multiplicationButton
            // 
            this.multiplicationButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.multiplicationButton.Enabled = false;
            this.multiplicationButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.multiplicationButton.FlatAppearance.BorderSize = 0;
            this.multiplicationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.multiplicationButton.Font = new System.Drawing.Font("Malgun Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiplicationButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.multiplicationButton.Location = new System.Drawing.Point(97, 282);
            this.multiplicationButton.Name = "multiplicationButton";
            this.multiplicationButton.Size = new System.Drawing.Size(85, 60);
            this.multiplicationButton.TabIndex = 13;
            this.multiplicationButton.Text = "*";
            this.multiplicationButton.UseVisualStyleBackColor = false;
            this.multiplicationButton.Click += new System.EventHandler(this.multiplicationButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.clearButton.Enabled = false;
            this.clearButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.clearButton.FlatAppearance.BorderSize = 0;
            this.clearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearButton.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.clearButton.Location = new System.Drawing.Point(8, 282);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(85, 60);
            this.clearButton.TabIndex = 20;
            this.clearButton.Text = "С";
            this.clearButton.UseVisualStyleBackColor = false;
            // 
            // listButton
            // 
            this.listButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.listButton.Enabled = false;
            this.listButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.listButton.FlatAppearance.BorderSize = 0;
            this.listButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.listButton.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listButton.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.listButton.Location = new System.Drawing.Point(8, 2);
            this.listButton.Name = "listButton";
            this.listButton.Size = new System.Drawing.Size(36, 46);
            this.listButton.TabIndex = 21;
            this.listButton.Text = "≣";
            this.listButton.UseVisualStyleBackColor = false;
            this.listButton.Click += new System.EventHandler(this.listButton_Click);
            // 
            // moreDetailedButton
            // 
            this.moreDetailedButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.moreDetailedButton.Enabled = false;
            this.moreDetailedButton.FlatAppearance.BorderColor = System.Drawing.Color.MediumBlue;
            this.moreDetailedButton.FlatAppearance.BorderSize = 0;
            this.moreDetailedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moreDetailedButton.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moreDetailedButton.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.moreDetailedButton.Location = new System.Drawing.Point(413, 2);
            this.moreDetailedButton.Name = "moreDetailedButton";
            this.moreDetailedButton.Size = new System.Drawing.Size(36, 46);
            this.moreDetailedButton.TabIndex = 22;
            this.moreDetailedButton.Text = "Δ";
            this.moreDetailedButton.UseVisualStyleBackColor = false;
            // 
            // hintsToolTip
            // 
            this.hintsToolTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.hintsToolTip.ForeColor = System.Drawing.Color.White;
            this.hintsToolTip.OwnerDraw = true;
            this.hintsToolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.hintsToolTip_Draw);
            // 
            // firstNumTxtBox
            // 
            this.firstNumTxtBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.firstNumTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.firstNumTxtBox.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNumTxtBox.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.firstNumTxtBox.Location = new System.Drawing.Point(8, 180);
            this.firstNumTxtBox.MaxLength = 20;
            this.firstNumTxtBox.Name = "firstNumTxtBox";
            this.firstNumTxtBox.Size = new System.Drawing.Size(441, 36);
            this.firstNumTxtBox.TabIndex = 23;
            this.firstNumTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.firstNumTxtBox.WordWrap = false;
            this.firstNumTxtBox.TextChanged += new System.EventHandler(this.firstNumTxtBox_TextChanged);
            this.firstNumTxtBox.Enter += new System.EventHandler(this.txtBox_Enter);
            this.firstNumTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBox_KeyPress);
            this.firstNumTxtBox.Leave += new System.EventHandler(this.txtBox_Leave);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Malgun Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.titleLabel.Location = new System.Drawing.Point(48, 15);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(281, 31);
            this.titleLabel.TabIndex = 24;
            this.titleLabel.Text = "Операции над числами";
            // 
            // firstNumLabel
            // 
            this.firstNumLabel.AutoSize = true;
            this.firstNumLabel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNumLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.firstNumLabel.Location = new System.Drawing.Point(11, 157);
            this.firstNumLabel.Name = "firstNumLabel";
            this.firstNumLabel.Size = new System.Drawing.Size(147, 28);
            this.firstNumLabel.TabIndex = 25;
            this.firstNumLabel.Text = "Первое число";
            // 
            // numSystemTxtBox
            // 
            this.numSystemTxtBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.numSystemTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numSystemTxtBox.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSystemTxtBox.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.numSystemTxtBox.Location = new System.Drawing.Point(8, 93);
            this.numSystemTxtBox.MaxLength = 2;
            this.numSystemTxtBox.Name = "numSystemTxtBox";
            this.numSystemTxtBox.Size = new System.Drawing.Size(36, 36);
            this.numSystemTxtBox.TabIndex = 26;
            this.numSystemTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numSystemTxtBox.WordWrap = false;
            this.numSystemTxtBox.TextChanged += new System.EventHandler(this.numSystemTxtBox_TextChanged);
            this.numSystemTxtBox.Enter += new System.EventHandler(this.txtBox_Enter);
            this.numSystemTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxNumSystem_KeyPress);
            this.numSystemTxtBox.Leave += new System.EventHandler(this.txtBox_Leave);
            // 
            // numSystemLabel
            // 
            this.numSystemLabel.AutoSize = true;
            this.numSystemLabel.Font = new System.Drawing.Font("Malgun Gothic", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSystemLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.numSystemLabel.Location = new System.Drawing.Point(10, 67);
            this.numSystemLabel.Name = "numSystemLabel";
            this.numSystemLabel.Size = new System.Drawing.Size(41, 30);
            this.numSystemLabel.TabIndex = 27;
            this.numSystemLabel.Text = "СС";
            // 
            // secondNumLabel
            // 
            this.secondNumLabel.AutoSize = true;
            this.secondNumLabel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondNumLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.secondNumLabel.Location = new System.Drawing.Point(11, 217);
            this.secondNumLabel.Name = "secondNumLabel";
            this.secondNumLabel.Size = new System.Drawing.Size(142, 28);
            this.secondNumLabel.TabIndex = 29;
            this.secondNumLabel.Text = "Второе число";
            // 
            // secondNumTxtBox
            // 
            this.secondNumTxtBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.secondNumTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.secondNumTxtBox.Font = new System.Drawing.Font("Malgun Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondNumTxtBox.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.secondNumTxtBox.Location = new System.Drawing.Point(8, 240);
            this.secondNumTxtBox.MaxLength = 20;
            this.secondNumTxtBox.Name = "secondNumTxtBox";
            this.secondNumTxtBox.Size = new System.Drawing.Size(441, 36);
            this.secondNumTxtBox.TabIndex = 28;
            this.secondNumTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.secondNumTxtBox.WordWrap = false;
            this.secondNumTxtBox.TextChanged += new System.EventHandler(this.secondNumTxtBox_TextChanged);
            this.secondNumTxtBox.Enter += new System.EventHandler(this.txtBox_Enter);
            this.secondNumTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBox_KeyPress);
            this.secondNumTxtBox.Leave += new System.EventHandler(this.txtBox_Leave);
            // 
            // resultTxtBox
            // 
            this.resultTxtBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.resultTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.resultTxtBox.Font = new System.Drawing.Font("Malgun Gothic", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultTxtBox.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.resultTxtBox.Location = new System.Drawing.Point(54, 83);
            this.resultTxtBox.MaxLength = 20;
            this.resultTxtBox.Name = "resultTxtBox";
            this.resultTxtBox.Size = new System.Drawing.Size(395, 50);
            this.resultTxtBox.TabIndex = 32;
            this.resultTxtBox.Text = "0";
            this.resultTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.resultTxtBox.WordWrap = false;
            this.resultTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.blockTxtBox_KeyPress);
            // 
            // miniTxtBox
            // 
            this.miniTxtBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.miniTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.miniTxtBox.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miniTxtBox.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.miniTxtBox.Location = new System.Drawing.Point(54, 67);
            this.miniTxtBox.MaxLength = 38;
            this.miniTxtBox.Name = "miniTxtBox";
            this.miniTxtBox.Size = new System.Drawing.Size(395, 23);
            this.miniTxtBox.TabIndex = 34;
            this.miniTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.miniTxtBox.WordWrap = false;
            this.miniTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.blockTxtBox_KeyPress);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(458, 352);
            this.Controls.Add(this.miniTxtBox);
            this.Controls.Add(this.resultTxtBox);
            this.Controls.Add(this.secondNumLabel);
            this.Controls.Add(this.secondNumTxtBox);
            this.Controls.Add(this.numSystemLabel);
            this.Controls.Add(this.numSystemTxtBox);
            this.Controls.Add(this.firstNumLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.firstNumTxtBox);
            this.Controls.Add(this.moreDetailedButton);
            this.Controls.Add(this.listButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.multiplicationButton);
            this.Controls.Add(this.minusButton);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.equelButton);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Components.EgoldsFormStyle egoldsFormStyle1;
        private System.Windows.Forms.Button equelButton;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button multiplicationButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button listButton;
        private System.Windows.Forms.Button moreDetailedButton;
        private System.Windows.Forms.ToolTip hintsToolTip;
        private System.Windows.Forms.TextBox firstNumTxtBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label firstNumLabel;
        private System.Windows.Forms.TextBox numSystemTxtBox;
        private System.Windows.Forms.Label numSystemLabel;
        private System.Windows.Forms.Label secondNumLabel;
        private System.Windows.Forms.TextBox secondNumTxtBox;
        private System.Windows.Forms.TextBox resultTxtBox;
        private System.Windows.Forms.TextBox miniTxtBox;
    }
}

