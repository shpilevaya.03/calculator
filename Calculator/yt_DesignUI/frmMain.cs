﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Calculator.Components;
using Calculator.Controls;

namespace Calculator
{
    public partial class frmMain : ShadowedForm
    {

        static class NamesOfComponents
        {
            //Buttons
            public static string ListButton =         "listButton";
            public static string MoreDetailedButton = "moreDetailedButton";

            //Labels
            public static string NumSystemLabel =  "numSystemLabel";

            //TextBoxes
            public static string FirstNumTxtBox =        "firstNumTxtBox";
            public static string NumSystemTxtBox =       "numSystemTxtBox";
            public static string SecondNumTxtBox =       "secondNumTxtBox";
        }

        static class Operation
        {
            public const string Equal =     "=";
            public const string Multiply =  "*";
            public const string Substract = "-";
            public const string Add =       "+";
        }

        List<char> Alphabet = new List<char>
        {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '~', '@', '#', '$', '%', '^', '&', '*', '<', '>', '\"', '\'', '\\', '`'
        };

        static string firstNumText =        "";
        static string secondNumText =       "";

        static int numSystem = 10;

        static string sign = Operation.Equal;

        public frmMain()
        {
            InitializeComponent();

            Control[] components = this.Controls.OfType<Control>().ToArray();
            foreach (var sender in components)
            {
                if (sender.Name == NamesOfComponents.ListButton)
                {
                    hintsToolTip.SetToolTip(sender, "Открыть навигацию");
                }
                else if (sender.Name == NamesOfComponents.MoreDetailedButton)
                {
                    hintsToolTip.SetToolTip(sender, "Посмотреть подробные вычисления");
                }
                else if (sender.Name == NamesOfComponents.NumSystemLabel)
                {
                    hintsToolTip.SetToolTip(sender, "Система счисления до 50");
                }
            }
        }

        private void listButton_Click(object sender, EventArgs e)
        {

        }

        private void hintsToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void txtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var chr = e.KeyChar;
            var text = ((Control)sender).Text;

            int num;
            var flag = int.TryParse(chr.ToString(), out num);

            if((text == "" && chr == '0') || (flag && num >= numSystem) || (!flag && chr != (char)Keys.Back && (!Alphabet.Contains(chr) || (Alphabet.IndexOf(chr) + 10 >= numSystem))))
                e.Handled = true;
        }

        private void txtBoxNumSystem_KeyPress(object sender, KeyPressEventArgs e)
        {
            string text = ((TextBox)sender).Text;
            char chr = e.KeyChar;

            if ((!Char.IsDigit(chr) && chr != (char)Keys.Back) || (Char.IsDigit(chr) && int.Parse(text + chr) > 50) || (chr == '0' && text == ""))
            {
                e.Handled = true;
            }
        }

        private void txtBox_Enter(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBox.BackColor = Color.CornflowerBlue;
        }

        private void txtBox_Leave(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBox.BackColor = Color.FromArgb(70, 70, 70);

            //if (firstNumText != "" && resultTxtBox.Text == "0")
            //{
            //    resultTxtBox.Text = firstNumText;
            //}
            //else if (secondNumText != "" && resultTxtBox.Text == "0")
            //{
            //    resultTxtBox.Text = secondNumText;
            //}
        }

        private void blockTxtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void firstNumTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (((Control)sender).Text != "")
                firstNumText = ((Control)sender).Text;
            else if (((Control)sender).Text == "")
                firstNumText = "0";

            if (sign == Operation.Equal)
                resultTxtBox.Text = firstNumText;
            else if (sign != Operation.Equal && miniTxtBox.Text != "")
                miniTxtBox.Text = firstNumText + $" {sign} ";
        }

        private void secondNumTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (((Control)sender).Text != "")
                secondNumText = ((Control)sender).Text;
            else if (((Control)sender).Text == "")
                secondNumText = "0";

            if (sign != Operation.Equal)
                resultTxtBox.Text = secondNumText;
        }

        private void multiplicationButton_Click(object sender, EventArgs e)
        {
            sign = Operation.Multiply;
            if(firstNumText != "")
                miniTxtBox.Text = firstNumText + $" {sign} ";
            if (secondNumText != "")
                resultTxtBox.Text = secondNumText;
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            sign = Operation.Substract;
            if (firstNumText != "")
                miniTxtBox.Text = firstNumText + $" {sign} ";
            if (secondNumText != "")
                resultTxtBox.Text = secondNumText;
        }

        private void plusButton_Click(object sender, EventArgs e)
        {
            sign = Operation.Add;
            if (firstNumText != "")
                miniTxtBox.Text = firstNumText + $" {sign} ";
            if (secondNumText != "")
                resultTxtBox.Text = secondNumText;
        }

        private void numSystemTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (((Control)sender).Text != "")
                numSystem = int.Parse(((Control)sender).Text);
            else
                numSystem = 10;
        }

        private void equelButton_Click(object sender, EventArgs e)
        {
            string result = "0";

            string output = "";
            int addition = 0;

            var length = firstNumText.Length > secondNumText.Length ? firstNumText.Length : secondNumText.Length;
            string min = firstNumText.Length < secondNumText.Length ? firstNumText : secondNumText;
            string max = firstNumText.Length >= secondNumText.Length ? firstNumText : secondNumText;
            min = new string(min.Reverse().ToArray()); max = new string(max.Reverse().ToArray());

            switch (sign)
            {
                case Operation.Add:
                    {
                        miniTxtBox.Text += secondNumText;

                        for (int i = 0; i < length; i++)
                        {
                            //цифры из первого числа
                            var first = max[i].ToString();
                            //цифры из второго числа
                            string second;
                            if (i < min.Length)
                                second = min[i].ToString();
                            else
                                second = "0";

                            int numMax, numMin;
                            if (!int.TryParse(first, out numMax))
                                numMax = Alphabet.IndexOf(max[i]) + 10;
                            if (!int.TryParse(second, out numMin))
                                numMin = Alphabet.IndexOf(min[i]) + 10;

                            if (numMax + numMin + addition < numSystem)
                            {
                                if (numMax + numMin + addition < 10)
                                    output = (numMax + numMin + addition) + output;
                                else
                                    output = Alphabet[numMax + numMin + addition - 10] + output;
                                addition = 0;
                            }
                            else
                            {
                                if (numMax + numMin + addition - numSystem < 10)
                                    output = ((numMax + numMin + addition) - numSystem) + output;
                                else
                                    output = Alphabet[(numMax + numMin + addition) - numSystem - 10] + output;
                                addition = 1;
                            }
                        }
                        if (addition > 0)
                            output = addition + output;
                        result = output;
                    }
                    break;

                case Operation.Substract:
                    {
                        miniTxtBox.Text += secondNumText;

                        for (int i = 0; i < length; i++)
                        {                            
                            //цифры из первого числа
                            var first = max[i].ToString();
                            //цифры из второго числа
                            string second;
                            if (i < min.Length)
                                second = min[i].ToString();
                            else
                                second = "0";

                            int numMax, numMin;
                            if (!int.TryParse(first, out numMax))
                                numMax = Alphabet.IndexOf(max[i]) + 10;
                            if (!int.TryParse(second, out numMin))
                                numMin = Alphabet.IndexOf(min[i]) + 10;

                            if (numMax - addition >= numMin)
                            {
                                if (numMax - addition - numMin < 10)
                                    output = (numMax - addition - numMin) + output;
                                else
                                    output = Alphabet[numMax - addition - numMin - 10] + output;
                                addition = 0;
                            }
                            else
                            {
                                if (Math.Abs(numMax + numSystem - addition - numMin) < 10)
                                    output = (numMax + numSystem - addition - numMin) + output;
                                else
                                    output = Alphabet[numMax + numSystem - addition - numMin - 10] + output;
                                addition = 1;
                            }
                        }

                        while(output.Length != 0 && output[0] == '0')
                            output = output.Remove(0, 1);
                        result = output.Length != 0 ? output : "0";
                    }
                    break;
                case Operation.Multiply:
                    miniTxtBox.Text += secondNumText;

                    string num = "";
                    for (int i = 0; i < length; i++)
                    {
                        //цифры из первого числа
                        var first = max[i].ToString();
                        //цифры из второго числа
                        string second = min[i].ToString();

                        int numMax, numMin;
                        if (!int.TryParse(first, out numMax))
                            numMax = Alphabet.IndexOf(max[i]) + 10;
                        if (!int.TryParse(second, out numMin))
                            numMin = Alphabet.IndexOf(min[i]) + 10;

                        for (int j = 0; j < min.Length; j++)
                        {
                            if ((numMax * numMin + addition) % numSystem < 10)
                                num = ((numMax * numMin + addition) % numSystem) + num;
                            else
                                num = Alphabet[((numMax * numMin + addition) % numSystem) - 10] + num;
                            addition = (numMax * numMin + addition) / numSystem;
                        }



                        result = output;
                    }

                    break;
                default:
                    break;
            }

            resultTxtBox.Text = result;
            sign = Operation.Equal;
        }
    }
}
